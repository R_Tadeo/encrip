-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-10-2020 a las 21:54:26
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cifrado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(7, '2016_06_01_000004_create_oauth_clients_table', 2),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('01bf57a2aa05e6e7e2db49012c8ab6322bd62b390fab25e07b829c36fbf8ae40c1503d771451c6bd', 3, 2, NULL, '[]', 0, '2020-10-03 22:31:12', '2020-10-03 22:31:12', '2021-10-03 17:31:12'),
('0562c081ff49d218842272f3f976c535542bb7a34d35e44c1b2c68c380338a7be938edddf7d1e1f4', 3, 2, NULL, '[]', 0, '2020-10-03 22:31:29', '2020-10-03 22:31:29', '2021-10-03 17:31:29'),
('0fbe0a296ae0e701262f4788dc2b1cfcaa57293c98ffc87b4d811b5fc7fa1efc42f7af5183541acd', 1, 2, NULL, '[]', 0, '2020-10-02 07:24:58', '2020-10-02 07:24:58', '2021-10-02 02:24:58'),
('24201cbb1d761dab12d3e0b9b95ed4d275b5ddd187d46b93538ba7c9576bdce4bae3656d8375c9be', 3, 2, NULL, '[]', 0, '2020-10-03 22:33:37', '2020-10-03 22:33:37', '2021-10-03 17:33:37'),
('4b5a0e02d2de5be238e99c6ef0e7b77846a62f96df33f7db7e0162f628a77099ac1342d7131010d6', 3, 2, NULL, '[]', 0, '2020-10-03 22:31:17', '2020-10-03 22:31:17', '2021-10-03 17:31:17'),
('529e345281ebafcd88c919adf4dfc115c3f5ed237f27f060c6d4b9846fa9e553f4e389725376bc9f', 3, 2, NULL, '[]', 0, '2020-10-03 22:31:03', '2020-10-03 22:31:03', '2021-10-03 17:31:03'),
('5b6d3a9e4145aaa81b183cec1491c6a3963ba956ddc8a75c3f8c85f75d38e1141a6bdef654352b1e', 3, 2, NULL, '[]', 0, '2020-10-03 22:31:26', '2020-10-03 22:31:26', '2021-10-03 17:31:26'),
('7b65627347eb619a1d45039887b234b24268b40147a54d82a8b35d77da67da44fd84b02bdba6f0de', 1, 2, NULL, '[]', 0, '2020-10-02 07:24:53', '2020-10-02 07:24:53', '2021-10-02 02:24:53'),
('86421a20b82b01b6acfe81faa8ed72e376d39d597750138d7107405398182785420e0be4a0e86ade', 3, 2, NULL, '[]', 0, '2020-10-03 22:33:34', '2020-10-03 22:33:34', '2021-10-03 17:33:34'),
('9209fe8a78df4f3643535b86dd6596e2e307dfc755c663761d27d0851fd951da65c72ed8f394b7ad', 3, 2, NULL, '[]', 0, '2020-10-03 22:33:41', '2020-10-03 22:33:41', '2021-10-03 17:33:41'),
('adf59099cadca9b0e8e5e6b01fd4f5338fbad1f768dad516a448d38097b71f69a06416a48e310f05', 3, 2, NULL, '[]', 0, '2020-10-03 22:31:24', '2020-10-03 22:31:24', '2021-10-03 17:31:24'),
('b87807f32d46483dfec032f027400d57c84294a03a20e4e1159abe6dfc49041558e88bf1af6d825b', 1, 2, NULL, '[]', 0, '2020-10-02 07:24:43', '2020-10-02 07:24:43', '2021-10-02 02:24:43'),
('df4b96db9968ad6d865e2d6f24c7688d6585bcffe38286c7f6921e013d81d9590df5e2605d1bd955', 3, 2, NULL, '[]', 0, '2020-10-03 22:31:10', '2020-10-03 22:31:10', '2021-10-03 17:31:10'),
('e0a5815c6ee37d446c00816b03f7fa05c64fc7f3243f8a4a57e6c9e09163da48624f24ee46246bc1', 1, 2, NULL, '[]', 0, '2020-10-02 07:24:56', '2020-10-02 07:24:56', '2021-10-02 02:24:56'),
('e44f169df7a8326bbfeb4bdc6727065abbba1f8c09cc87c8d2d3353e466e02f5ed45eca8a3e52916', 3, 2, NULL, '[]', 0, '2020-10-03 22:30:42', '2020-10-03 22:30:42', '2021-10-03 17:30:42'),
('f908e868806f449eec6e72bb64b10f51e5e346e455e20d35e5dd6c3393a5a278e9d83e1593ac98db', 3, 2, NULL, '[]', 0, '2020-10-03 22:31:42', '2020-10-03 22:31:42', '2021-10-03 17:31:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'j4wWWfRraeZlSCPY4nIzebrfa5lumYLALw9hIiy6', NULL, 'http://localhost', 1, 0, 0, '2020-10-02 07:08:43', '2020-10-02 07:08:43'),
(2, NULL, 'Laravel Password Grant Client', 'evx5x5mKuEPnTX3gYtZFGMaKG0VCnSDNkC4HLpO3', 'users', 'http://localhost', 0, 1, 0, '2020-10-02 07:08:43', '2020-10-02 07:08:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-10-02 07:08:43', '2020-10-02 07:08:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('196d180367ef84e1963b5665732331ce8ec5b8909bd66f4682c344bf8b11d1513ba11af7895da46c', '4b5a0e02d2de5be238e99c6ef0e7b77846a62f96df33f7db7e0162f628a77099ac1342d7131010d6', 0, '2021-10-03 17:31:17'),
('1f63761e8e85754c4114003ec08bdd48586424df53dd8cb574e2e7af8b21a0334855c7c4b5089797', 'b87807f32d46483dfec032f027400d57c84294a03a20e4e1159abe6dfc49041558e88bf1af6d825b', 0, '2021-10-02 02:24:43'),
('3fcaf82c1c446ae9ba3e552a0edd41cdb6c1d64c676249c51ddb9ce2d47e749d3d849085fc3f5693', '0fbe0a296ae0e701262f4788dc2b1cfcaa57293c98ffc87b4d811b5fc7fa1efc42f7af5183541acd', 0, '2021-10-02 02:24:58'),
('56d0f146a756dbb7fdf29741c12acf7d79551839c94c491fb4da2caeb303dfe3661091adfd0e40f7', '5b6d3a9e4145aaa81b183cec1491c6a3963ba956ddc8a75c3f8c85f75d38e1141a6bdef654352b1e', 0, '2021-10-03 17:31:26'),
('5c4fa9a397974d1d750f0dead5a45184a0c46e8e7a158ec16a4e647c0600eaf08493136537c095d6', 'df4b96db9968ad6d865e2d6f24c7688d6585bcffe38286c7f6921e013d81d9590df5e2605d1bd955', 0, '2021-10-03 17:31:10'),
('5c7bc2b985d0ef319fd663b2032bf2b55733ce290cfa1d13ac4d4303cae181ebb1681bdfcafbbd95', '7b65627347eb619a1d45039887b234b24268b40147a54d82a8b35d77da67da44fd84b02bdba6f0de', 0, '2021-10-02 02:24:53'),
('6007376916e43fbeb62295751596d39414e48228cb77aa22841c87220e9ec6fbbc5fe504c1e04738', '9209fe8a78df4f3643535b86dd6596e2e307dfc755c663761d27d0851fd951da65c72ed8f394b7ad', 0, '2021-10-03 17:33:41'),
('858ff1a685cbd5279d8bba325e729b8b3f126e1dae9e0d5e85ae4d7800a1b081d9614e6099116afa', 'adf59099cadca9b0e8e5e6b01fd4f5338fbad1f768dad516a448d38097b71f69a06416a48e310f05', 0, '2021-10-03 17:31:24'),
('859f7c5db86c7f6541acbfdfbdd3e1d65fdb70c5cfc8a9376eeb2e90528f354279bc951fdb788187', 'f908e868806f449eec6e72bb64b10f51e5e346e455e20d35e5dd6c3393a5a278e9d83e1593ac98db', 0, '2021-10-03 17:31:42'),
('88a389bf67b1a8ee40e4819da35f4091b2cc0ff27c4f893a42aa0b5093cc1a04825465a50544387f', 'e0a5815c6ee37d446c00816b03f7fa05c64fc7f3243f8a4a57e6c9e09163da48624f24ee46246bc1', 0, '2021-10-02 02:24:56'),
('8997ccfc1a5b4ca9a3f393c0cb22a6beb1edacac5fd3c5d15ae5487a1fc121fdbfb75bdc2c25ed4e', '01bf57a2aa05e6e7e2db49012c8ab6322bd62b390fab25e07b829c36fbf8ae40c1503d771451c6bd', 0, '2021-10-03 17:31:12'),
('95b09f1ad31b5f7cd4fdee09ac0608796959bdc9da0c3e39613eb009e81763bb184fd0eec311f6ce', '86421a20b82b01b6acfe81faa8ed72e376d39d597750138d7107405398182785420e0be4a0e86ade', 0, '2021-10-03 17:33:34'),
('d52ff98a5eae4fd112f1683cd0014e438104229778ccb25c174c9ff5d40a722d7225bbfa965270fb', '0562c081ff49d218842272f3f976c535542bb7a34d35e44c1b2c68c380338a7be938edddf7d1e1f4', 0, '2021-10-03 17:31:29'),
('dfa05cb139bdac2e2018518382787e68b1211d85ea49c271256824e2dab9207ed7c40f46d5e0d610', '24201cbb1d761dab12d3e0b9b95ed4d275b5ddd187d46b93538ba7c9576bdce4bae3656d8375c9be', 0, '2021-10-03 17:33:37'),
('e80c88f6a8e2d5489aba7276149582f10163f42fb425bbca95c94fc642044ce302aebfc699c1f6d6', 'e44f169df7a8326bbfeb4bdc6727065abbba1f8c09cc87c8d2d3353e466e02f5ed45eca8a3e52916', 0, '2021-10-03 17:30:42'),
('edcc1fb8f308f85febcddf422dc4974cd463deb76678458416a4c769da63cf11aea5d8907551b1bb', '529e345281ebafcd88c919adf4dfc115c3f5ed237f27f060c6d4b9846fa9e553f4e389725376bc9f', 0, '2021-10-03 17:31:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'RICARDO MOLLINEDO TADEO', 'tikotadeo@gmail.com', NULL, '$2y$10$BYGs8phgN0DaDzW4MG6MMOsGuTE1MXiKD9jHTW8JgNIIIxzbhxXES', NULL, '2020-10-02 06:56:23', '2020-10-02 06:56:23'),
(3, 'Juan perez', 'jueanperez@gmail.com', NULL, '$2y$10$W6pI99nc7XRHQ2wVKVMqme5iix288H280MA7p7ARgrz9asTq5whDe', NULL, '2020-10-03 22:26:47', '2020-10-03 22:26:47');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
